winget install Git.Git

# add path
if (Test-Path $profile){
    Add-Content $profile '$ENV:Path+=";C:\Program Files\Git\bin"'
}else{
    New-Item -Path $profile -Type file -Force
    Add-Content $profile '$ENV:Path+=";C:\Program Files\Git\bin"'
}

git config --global init.defaultBranch main

# register user info (fill either)
git config --global user.email "you@example.com"
git config --global user.name "Your Name"

# if you use proxy:
git config --global http.proxy http://example.jp:8080
