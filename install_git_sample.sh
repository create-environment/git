#!/bin/bash

sudo apt install git # debian
sudo dnf install git # RHEL
sudo zypper in git # SUSE

git config --global init.defaultBranch main

# register user info (fill either)
git config --global user.email "you@example.com"
git config --global user.name "Your Name"

# if you use proxy:
git config --global http.proxy http://example.jp:8080
